# Kibana dashboards

Collection of my personal Kibana dashboards.

## Licence

MIT Licence

## Author

Radek Sprta <mail@radeksprta.eu>
